package internal

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/pcanilho/go-jira"
)

type jiraController struct {
	client   *jira.Client
	ctx      context.Context
	username string
}

const (
	FieldEpiclink = "customfield_10900"
	FieldEpicName = "customfield_10901"
)

func NewJira(ctx context.Context, username, password, url string) (*jiraController, error) {
	jiraHttpClient := jira.BasicAuthTransport{
		Username: username,
		Password: password,
	}

	client, err := jira.NewClient(jiraHttpClient.Client(), url)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to authenticate with the Jira instance at [%s]", url)
	}

	if ctx == nil {
		ctx = context.Background()
	}
	return &jiraController{ctx: ctx, client: client, username: username}, nil
}
