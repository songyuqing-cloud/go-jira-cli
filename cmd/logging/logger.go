package logging

import (
	nested "github.com/antonfisher/nested-logrus-formatter"
	joonix "github.com/joonix/log"
	logger "github.com/sirupsen/logrus"
	"io/ioutil"
	"time"
)

const (
	fileRotationMaxSize    = 100 // MB
	fileRotationMaxBackups = 5
	fileRotationMaxAge     = 31 // Monthly
)

func SetupLogger(verbosityLevel int, reportFilePath string) {
	// Generic logger
	logger.SetLevel(logger.TraceLevel)
	// Only hooks are responsible for the printing
	logger.SetOutput(ioutil.Discard)

	// stdout formatter
	stdoutFormatter := &nested.Formatter{
		HideKeys:        true,
		ShowFullLevel:   true,
		TimestampFormat: time.RFC3339,
	}

	// file formatter
	fileLogFormatter := joonix.NewFormatter(joonix.StackdriverFormat)

	// file-hook formatter
	applicationHook := NewApplicationHook(ApplicationHookConfig{
		Filename:   "log/app.log",
		MaxSize:    fileRotationMaxSize,
		MaxBackups: fileRotationMaxBackups,
		MaxAge:     fileRotationMaxAge,
		Level:      logger.Level(verbosityLevel),
		Formatter:  fileLogFormatter,
	})

	// Hooks
	logger.AddHook(NewStdoutHook(logger.Level(verbosityLevel), stdoutFormatter))
	logger.AddHook(applicationHook)
}
