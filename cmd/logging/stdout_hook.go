package logging

import (
	logger "github.com/sirupsen/logrus"
	"io"
	"os"
)

type StdoutHook struct {
	Level logger.Level
	Formatter logger.Formatter
}

func NewStdoutHook(level logger.Level, formatter logger.Formatter) logger.Hook {
	return StdoutHook{level, formatter}
}

func (s StdoutHook) Levels() []logger.Level {
	return logger.AllLevels
}

func (s StdoutHook) Fire(entry *logger.Entry) error {
	if s.Level >= entry.Level {
		b, err := s.Formatter.Format(entry)
		if err != nil {
			return err
		}
		if _, err = io.WriteString(os.Stdout, string(b)); err != nil {
			return err
		}
	}
	return nil
}
