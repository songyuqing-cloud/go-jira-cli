package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	cliErrors "gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"strings"
)

var addCmd = &cobra.Command{
	Use:          "add",
	Short:        "Specifies that property is to be added to the issue",
	PreRun:       func(cmd *cobra.Command, args []string) { OverrideArgsFromEnv() },
	SilenceUsage: true,
}

var commentCmd = &cobra.Command{
	Use:    "comment",
	Short:  "Add a comment to the desired issue",
	PreRun: func(cmd *cobra.Command, args []string) { OverrideArgsFromEnv() },
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify the issue identifier to update")
		}
		if len(strings.TrimSpace(commentBody)) == 0 {
			return fmt.Errorf("please specify a message to be added as a comment to the ticket")
		}
		comment, err := jiraController.AddCommentToIssue(args[0], commentBody)
		if err != nil {
			return cliErrors.NewCliError(err)
		}
		fmt.Println(comment.ID)
		return nil
	},
}

func init() {
	// Comment
	commentCmd.Flags().StringVarP(&commentBody, "body", "b", "", "the comment body to be added to the issue")
	// Add sub-commands
	addCmd.AddCommand(commentCmd)
}
