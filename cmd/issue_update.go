package cmd

import (
	cliErrors "gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"gitlab.com/pcanilho/go-jira-cli/cmd/helpers"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/go-jira"
	"os"
	"strings"
)

var updateCmd = &cobra.Command{
	Use:          "update",
	Short:        "Updates one or multiple issues based on the provided identifiers",
	PreRun:       func(cmd *cobra.Command, args []string) { OverrideArgsFromEnv() },
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify the issue identifier to update")
		}
		for _, issueID := range args {
			issue, err := jiraController.GetIssue(issueID, nil)
			if err != nil {
				return cliErrors.NewCliError(err)
			}

			// Update issue with the provided flags
			toAttach, changelog, err := updateIssueAttributes(issue)
			if err != nil {
				return cliErrors.NewCliError(err)
			}

			// Nothing to update
			if changelog.IsEmpty() {
				helpers.ExitWithMessage(fmt.Sprintf("No change was detected for the ticket {key:[%s], summary: [%s]}...",
					issue.Key, issue.Fields.Summary))
			}

			// Prompt the user before continuing
			if !skipPrompts {
				fmt.Println(changelog)
				confirmed, _ := helpers.PromptToContinue()

				// Abort operation if not confirmed by the user
				if !confirmed {
					helpers.ExitWithAbort()
				}
			}

			// Update issue fields
			_, err = jiraController.UpdateIssue(issue, changelog.ToMap())
			if err != nil {
				return cliErrors.NewCliError(err)
			}

			// Upload attachments
			if err = jiraController.UploadAttachmentsToIssue(issue, toAttach...); err != nil {
				return cliErrors.NewCliError(err)
			}
			fmt.Println(issue.Key)
		}
		return nil
	},
}

func init() {
	// Update
	updateCmd.Flags().StringVarP(&summary, "summary", "s", "", "the new issue summary")
	updateCmd.Flags().StringArrayVarP(&attachments, "attachment", "a", nil, "attachment(s) to be added to the issue")
	updateCmd.Flags().StringVarP(&epicLink, "epic-link", "e", "", "the epic link")

	// Add sub-commands
	updateCmd.AddCommand(addCmd)
}

func updateIssueAttributes(issue *jira.Issue) ([]*internal.IssueAttachment, *helpers.ChangeLog, error) {
	changeLog := helpers.NewChangeLog()

	// Summary
	if len(strings.TrimSpace(summary)) > 0 {
		changeLog.AddChange("summary", issue.Fields.Summary, summary, true, true)
	}

	// Epic
	epicLink = strings.TrimSpace(epicLink)
	if len(epicLink) > 0 {
		// Does the provided epic exist?
		epicIssue, err := jiraController.GetEpicLink(epicLink)
		if err != nil {
			return nil, changeLog, err
		}

		var currentEpic string
		if issue.Fields.Epic != nil {
			currentEpic = issue.Fields.Epic.Name
		}

		currentEpicName, err := epicIssue.Fields.Unknowns.String(internal.FieldEpicName)
		if err == nil && currentEpic != currentEpicName {
			changeLog.AddChange("Epic Link", currentEpic, epicLink, false, true)
			changeLog.AddChange(internal.FieldEpiclink, "", epicIssue.Key, true, false)
		}
	}

	// Attachments
	var issueAttachments []*internal.IssueAttachment
	for _, a := range attachments {
		changeLog.AddChange("Attachment", "", a, false, true)
		var name, path string
		raw := strings.Split(a, ":")
		if len(raw) > 1 {
			name, path = raw[0], raw[1]
		} else {
			name, path = raw[0], raw[0]
		}

		attachment, err := attachmentFromPath(name, path)
		if err != nil {
			return nil, changeLog, err
		}

		issueAttachments = append(issueAttachments, attachment)
	}
	return issueAttachments, changeLog, nil
}

func attachmentFromPath(name, path string) (*internal.IssueAttachment, error) {
	// File leak (sdk doesn't close the resource) ?!?!?
	fi, err := os.Stat(path)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to state file at [%s]", path)
	}
	if fi.IsDir() {
		return nil, fmt.Errorf("the path [%s] is a directory, a file must be provided instead", path)
	}

	f, err := os.Open(path)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to open the file at [%s]", path)
	}
	return &internal.IssueAttachment{
		Reader:   f,
		Filename: name,
	}, nil
}
