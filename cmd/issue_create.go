package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	cliErrors "gitlab.com/pcanilho/go-jira-cli/cmd/errors"
	"gitlab.com/pcanilho/go-jira-cli/internal"
	"strings"
)

var createCmd = &cobra.Command{
	Use:    "create",
	Short:  "Creates a Jira ticket",
	PreRun: func(cmd *cobra.Command, args []string) { OverrideArgsFromEnv() },
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(strings.TrimSpace(clone)) != 0 {
			// Redirect to the cloneCmd
			return cloneCmd.RunE(cmd, args)
		}

		if len(strings.TrimSpace(project)) == 0 {
			return fmt.Errorf("please specify the ticket project")
		}
		if len(strings.TrimSpace(summary)) == 0 {
			return fmt.Errorf("please specify the ticket summary")
		}
		if len(strings.TrimSpace(issueType)) == 0 {
			return fmt.Errorf("please specify the ticket type")
		}

		// Compose the creation options
		creationOpts := &internal.IssueCreationOptions{
			Summary:      summary,
			Description:  description,
			Project:      project,
			IssueType:    issueType,
		}

		// Validate, if provided, that the [Epic Link] value exists
		if len(strings.TrimSpace(epicLink)) != 0 {
			eIssue, err := jiraController.GetEpicLink(epicLink)
			if err != nil {
				return err
			}
			creationOpts.CustomFields.Set(internal.FieldEpiclink, eIssue.Key)
		}

		// Add additional custom fields
		fieldsToAdd, err := getFieldFromArray(customFields)
		if err != nil {
			return cliErrors.NewCliError(err)
		}

		for k, v := range fieldsToAdd {
			creationOpts.CustomFields.Set(k, v)
		}

		issue, err := jiraController.CreateIssue(creationOpts)
		if err != nil {
			return cliErrors.NewCliError(err)
		}
		fmt.Println(issue.Key)
		return nil
	},
}

func init() {
	createCmd.Flags().StringVar(&clone, "clone", "", "use a ticket as a base template")
	createCmd.Flags().StringVarP(&project, "project", "p", "", "the ticket project (required when not cloning)")
	createCmd.Flags().StringVarP(&issueType, "type", "t", "", "the ticket type (required when not cloning)")
	createCmd.Flags().StringVarP(&summary, "summary", "s", "", "the ticket summary (required when not cloning)")
	createCmd.Flags().StringVarP(&description, "description", "d", "", "the ticket description (optional)")
	createCmd.Flags().StringVarP(&description, "epic-link", "e", "", "the ticket epic (optional)")
	createCmd.Flags().StringArrayVarP(&customFields, "custom-field", "c", nil, "custom fields to attach to the ticket (optional)")
}
