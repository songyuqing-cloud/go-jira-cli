package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

var getCmd = &cobra.Command{
	Use:    "get",
	Short:  "Retrieves multiple issues based on the provided identifiers",
	PreRun: func(cmd *cobra.Command, args []string) { OverrideArgsFromEnv() },
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify the issue identifier to retrieve details from")
		}
		for _, issueID := range args {
			issue, err := jiraController.GetIssue(issueID, nil)
			if err != nil {
				return nil
			}
			fmt.Printf("%+v\n", issue)

		}
		return nil
	},
}
