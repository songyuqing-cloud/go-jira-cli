package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/go-jira"
	"gitlab.com/pcanilho/go-jira-cli/cmd/errors"
)

var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "Searches for a Jira ticket based on the supplied JQL query",
	Example: fmt.Sprintf(`
# Search for issues created in the last 4 weeks and sort by created date
%s search 'created >= -4w ORDER BY created DESC'`, name),
	PreRun:       func(cmd *cobra.Command, args []string) { OverrideArgsFromEnv() },
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify a JQL query")
		}
		issues, err := jiraController.SearchIssues(args[0], composeSearchOptions(), maxOccurrences)
		if err != nil {
			return errors.NewCliError(err)
		}

		for _, i := range issues {
			fmt.Println(i.Key)
		}
		return nil
	},
}

var searchByNamePatternCmd = &cobra.Command{
	Use:   "name",
	Short: "Searches for a Jira ticket based on the supplied name pattern using regular-expression syntax",
	RunE: func(cmd *cobra.Command, args []string) error {
		return nil
		//issues, err := jiraController.SearchIssues()
	},
}

func init() {
	searchCmd.PersistentFlags().IntVarP(&limitResults, "limit", "l", 0, "limit the amount of issues to be retrieved (defaults to all)")
	searchCmd.PersistentFlags().IntVarP(&maxOccurrences, "max-output", "m", 0, "raises an error if the total output amount exceeds the provided value (defaults to all)")
}

func composeSearchOptions() *jira.SearchOptions {
	return &jira.SearchOptions{
		MaxResults: limitResults,
	}
}
