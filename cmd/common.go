package cmd

import (
	"fmt"
	"os"
	"strings"
)

/****************** Flags ******************/

var (
	description, project, issueType string   // Create & Clone
	clone                           string   // Create
	summary, epicLink               string   // Update & Create
	attachments                     []string // Update
	commentBody                     string   // Update -> Add -> Comment
	limitResults, maxOccurrences    int      // Search
	customFields                    []string // Common
)

func isInPipe() bool {
	fileStat, _ := os.Stdin.Stat()
	return fileStat.Mode()&os.ModeCharDevice == 0
}

func getFieldFromArray(fields []string) (map[string]interface{}, error) {
	out := make(map[string]interface{})
	for _, f := range fields {
		raw := strings.Split(f, ":")
		if len(raw) != 2 {
			return nil, fmt.Errorf("field string must be colon separated, e.g. 'name:value'")
		}
		out[raw[0]] = raw[1]
	}
	return out, nil
}
