# go-jira-cli (`jira-cli`)

## Features

* Authentication (via the standard Jira SDK available [here](https://github.com/andygrunwald/go-jira))
* CRUD (`Create`, `Read`, `Update` & `Delete`) operations on issues via Key or Identifier.
* Search tickets by supplying a `JQL` query. Searches can also be asserted to return only a specified number of rows as 
to ascertain that piped commands work as expected.
* Full support for piped commands (`xargs`) via the `-f` or `--force` global flags.

### Note
This project does not wrap the entire functionality of the Jira API. It is being extended on a per-need basis. 

## Requirements

* Go >= `1.14`
* Jira `v6.3.4` to `v7.1.2`.
* `direnv` (optional)

## Installation

Via `go get`:

```bash
$ go get gitlab.com/pcanilho/go-jira-cli
```

Using it as a library:

```go
package main

import (
    jira_cli "gitlab.com/pcanilho/go-jira-cli"
)
...
```

## Configuration

The authentication secrets used to interact with the Jira API can be provided either via arguments to the applications,
as seen below, or via environment variables. For the latter approach, I fully recommend the usage of `direnv` allied
with a `.env` file so that secrets can be loaded automatically.

* Via arguments:
```shell
Global Flags:
      --jira.password string   [JIRA_PASSWORD] the password used to authenticate with Jira
      --jira.url string        [JIRA_URL] the Jira instance URL
      --jira.username string   [JIRA_USERNAME] the username used to authenticate with Jira
```

* Via `direnv` + `.env` file:

1. Copy the sample environment file:

```shell
$ cp env.sample .env
``` 

2. Overwrite the values with the ones appropriate to your use-case.
3. Allow `direnv` to extract those values whenever you visit the folder that has the previously created `.env` file:

```shell
$ direnv allow
direnv: export +JIRA_PASSWORD +JIRA_URL +JIRA_USERNAME
```

## Interfaces

Internally, I created the `Controller` interface to more easily declare which features are available via this project. 
It can be used as a starting point for any extensions. However, I suggest the usage/creation of other interfaces that translate additional 
functionality as to mitigate upstream conflicts.

`internal/controller.go`:
```go
type Controller interface {
	IssueController
	...
}

type IssueController interface {
	CreateIssue(*IssueCreationOptions) (*jira.Issue, error)
	CloneIssue(interface{}) (*jira.Issue, error)
	SearchIssues(string, *jira.SearchOptions, int) ([]jira.Issue, error)
	GetIssue(string, *jira.GetQueryOptions) (*jira.Issue, error)
	UpdateIssue(interface{}, map[string]interface{}) (*jira.Issue, error)
	AddCommentToIssue(interface{}, string) (*jira.Comment, error)
	UploadAttachmentsToIssue(interface{}, ...*IssueAttachment) error
	DeleteIssue(interface{}) error
}
```

## Examples

### Searching for tickets keys using a `JQL` query

Via `cli`:

```shell
$ jira-cli issue search 'created >= -4w ORDER BY created DESC' 
```

Via `library`:

```go
jql := "created >= -4w ORDER BY created DESC"
if issues, err := jiraController.SearchIssues(jql, nil, 0); err != nil {
	return err
} else {
	for _, issue := range issues {
	    fmt.Println(issue.Key)	
    }   
}
```
